package me.julian.breadvote;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.julian.breadvote.commands.VoteBan;
import me.julian.breadvote.commands.VotePardon;

public class Main extends JavaPlugin{
	@Override
	public void onEnable() {
		VoteManager.init(this);
		this.getCommand("voteban").setExecutor(new VoteBan());
		this.getCommand("votepardon").setExecutor(new VotePardon());
		Bukkit.getPluginManager().registerEvents(new Listeners(), this);
	}
}
