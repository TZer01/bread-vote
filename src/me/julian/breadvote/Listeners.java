package me.julian.breadvote;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Listeners implements Listener{
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent e) {
		VoteManager.update();
	}
	@EventHandler
	public void onPlayerQuitEvent(PlayerQuitEvent e) {
		VoteManager.update();
	}
}
