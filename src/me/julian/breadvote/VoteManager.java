package me.julian.breadvote;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.BanList.Type;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class VoteManager {
	private static Map<String, Vote> banVotes = new HashMap<String, Vote>();
	private static Map<String, Vote> pardonVotes = new HashMap<String, Vote>();
	private static JavaPlugin main;
	public static void init(JavaPlugin m) {
		main = m;
	}
	public static Vote getBanVote(String player, String sender) {
		if(Bukkit.getBanList(Type.NAME).isBanned(player)) {
			return null;
		}
		if(!banVotes.containsKey(player)) {
			Bukkit.broadcastMessage("Vote started by " + sender + " to ban " + player);
			Bukkit.broadcastMessage("Type /voteban " + player + " to help ban them");
			banVotes.put(player, new BanVote(main, 0.5, player));
		}
		return banVotes.get(player);
	}
	public static Vote getPardonVote(String player, String sender) {
		if(!Bukkit.getBanList(Type.NAME).isBanned(player)) {
			return null;
		}
		if(!pardonVotes.containsKey(player)) {
			Bukkit.broadcastMessage("Vote started by " + sender + " to pardon " + player);
			Bukkit.broadcastMessage("Type /votepardon " + player + " to help pardon them");
			pardonVotes.put(player, new PardonVote(main, 0.5, player));
		}
		return pardonVotes.get(player);
	}
	public static void update() {
		Iterator<String> i = banVotes.keySet().iterator();
		while(i.hasNext()) {
			banVotes.get(i.next()).update();
		}
		i = pardonVotes.keySet().iterator();
		while(i.hasNext()) {
			pardonVotes.get(i.next()).update();
		}
	}
	public static void removeBanVote(String player) {
		banVotes.remove(player);
	}
	public static void removePardonVote(String player) {
		pardonVotes.remove(player);
	}
}