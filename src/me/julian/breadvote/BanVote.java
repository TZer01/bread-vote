package me.julian.breadvote;

import org.bukkit.BanList.Type;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class BanVote extends Vote{
	String player;
	public BanVote(JavaPlugin m, double threshold, String player) {
		super(m, threshold);
		this.player = player;
	}
	@Override
	public void voteSuccess() {
		super.voteSuccess();
		Bukkit.getBanList(Type.NAME).addBan(player, "Banned by consensus", null, "Democracy");
		Player kick = Bukkit.getPlayer(player);
		if(kick != null) {
			kick.kickPlayer("You have been banned through democracy");
		}
		Bukkit.broadcastMessage(player + " banned through democracy!");
	}
	@Override
	public void voteFail() {
		super.voteFail();
		Bukkit.broadcastMessage(player + " ban vote failed");
	}
	@Override
	public void progress(String cur, String total) {
		Bukkit.broadcastMessage(player + " ban vote (" + cur + "/" + total + ")");
	}
	@Override
	public void remove() {
		VoteManager.removeBanVote(player);
	}
}
