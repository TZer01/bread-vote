package me.julian.breadvote;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Vote {
	List<UUID> voted = new ArrayList<UUID>();
	double threshold;
	public Vote(JavaPlugin main, double threshold) {
		this.threshold = threshold;
		Bukkit.getScheduler().runTaskLater(main, new Runnable() {
            @Override
            public void run() {
            	if(!success) {
            		voteFail();
            	}
            }
        }, 20L*60L);
	}
	public boolean addVote(Player player) {
		if(!voted.contains(player.getUniqueId())) {
			voted.add(player.getUniqueId());
			update();
			return true;
		}
		return false;
	}
	private boolean success = false;
	public void voteSuccess() {
		success = true;
		remove();
	}
	public void voteFail() {
		remove();
	}
	public void remove() {
		
	}
	public void progress(String cur, String total) {
		Bukkit.broadcastMessage("Vote (" + cur + "/" + total + ")");
	}
	public void update() {
		if(voted.size() > (Bukkit.getOnlinePlayers().size())*threshold) {
			voteSuccess();
		}else if(voted.size() > 1){
			progress(Integer.toString(voted.size()), Integer.toString(1+(int)((Bukkit.getOnlinePlayers().size())*threshold)));
		}
	}
}
