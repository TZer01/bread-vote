package me.julian.breadvote;

import org.bukkit.BanList.Type;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class PardonVote extends Vote{
	String player;
	public PardonVote(JavaPlugin m, double threshold, String player) {
		super(m, threshold);
		this.player = player;
	}
	@Override
	public void voteSuccess() {
		super.voteSuccess();
		Bukkit.getBanList(Type.NAME).pardon(player);
		Bukkit.broadcastMessage(player + " pardoned through democracy!");
	}
	@Override
	public void voteFail() {
		super.voteFail();
		Bukkit.broadcastMessage(player + " pardon vote failed");
	}
	@Override
	public void progress(String cur, String total) {
		Bukkit.broadcastMessage(player + " pardon vote (" + cur + "/" + total + ")");
	}
	@Override
	public void remove() {
		VoteManager.removePardonVote(player);
	}
}
