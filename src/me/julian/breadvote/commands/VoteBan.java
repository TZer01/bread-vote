package me.julian.breadvote.commands;

import me.julian.breadvote.Vote;
import me.julian.breadvote.VoteManager;

public class VoteBan extends VoteCommand{
	public VoteBan() {
		super();
		inableMessage = "That player is already banned, silly.";
	}
	@Override
	public Vote getVote(String player, String sender) {
		return VoteManager.getBanVote(player, sender);
	}
}
