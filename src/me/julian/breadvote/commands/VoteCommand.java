package me.julian.breadvote.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.julian.breadvote.Vote;

public class VoteCommand implements CommandExecutor{
	// voteban <player>
	String inableMessage = "INABLE MESSAGE";
	int minPlayers = 7;
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("You need to be a player to run this command!");
			return false;
		}
		if(args.length != 1) {
			return false;
		}
		if(Bukkit.getOnlinePlayers().size() < minPlayers) {
			sender.sendMessage(Integer.toString(minPlayers) + " players need to be online for democracy to happen.");
			return true;
		}
		Vote vote = getVote(args[0], sender.getName());
		if(vote == null) {
			sender.sendMessage(inableMessage);
			return true;
		}
		vote.addVote((Player)sender);
		return true;
	}
	public Vote getVote(String name, String sender) {
		return null;
	}
}