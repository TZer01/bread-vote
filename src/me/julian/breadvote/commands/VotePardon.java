package me.julian.breadvote.commands;

import me.julian.breadvote.Vote;
import me.julian.breadvote.VoteManager;

public class VotePardon extends VoteCommand{
	public VotePardon() {
		super();
		inableMessage = "That player isn't banned, silly.";
	}
	@Override
	public Vote getVote(String player, String sender) {
		return VoteManager.getPardonVote(player, sender);
	}
}
